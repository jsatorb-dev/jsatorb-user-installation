# JSatOrb project: VTS complementary documentation for JSatOrb users

This file describes the use of VTS with JSatOrb.


## VTS visualization

The VTS agent opens VTS and the visualization begins with "Run", as shown here:   

![Run VTS](../images/run_vts.png)  

Then, four windows are opened:

- **Bottom left:** the broker, which is the main controller. Time can be controlled in the "Timeline" thumbnail, and properties of the different applications in the "View Properties" thumbnail.
- **Top left:** Celestia, with a 3D visualization of the central body and the satellites. The view can be rotated by moving the mouse while clicking on the right button, or in the broker's "3D Cameras" thumbnail.
- **Top right:** SurfaceView, with a 2D visualization of the central body, the satellites, and the ground stations. The terminator (frontier between sunlit and dark surfaces) is also showed, as well as the satellites' traces and the regions seen by the ground stations at a certain altitude (average altitude between all satellites).
- **Bottom right:** PrestoPlot, for data visualization. One or several lines can be selected, and then visualization can be launched with a right click and selection of "New Graph". Raw data can be simultaneously available with the button "Display the data values", with an emphasis on the values corresponding to the current selected time. 

![PrestoPlot Data](../images/prestoplot_data.png)

At the end, close the broker to close all windows, and then close the VTS main window.

Options:

- Keplerian data: data can be visualized with PrestoPlot.
- LLA data: data can be visualized with PrestoPlot.
- Eclipse: intervals can be visualized in the broker under the timeline, in the "Timeline" thumbnail.   

![Eclipse Visibility Broker](../images/eclipse_visibility_broker.png)
- Visibility: intervals can be visualized in the broker under the timeline, in the "Timeline" thumbnail. Moreover, in Celestia and SurfaceView, the trajectories and traces are white when the satellite is visible by a ground station.   

![Visibility SurfaceView](../images/visibility_surfaceview.png)


## VTS coverage visualization

For coverage visualization, only two windows are opened: the broker and SurfaceView. 
The coverage plot consists of a .png layer ontop of the body's surface in SurfaceView.  
The opacity of this .png layer can be selected in the "View Properties" thumbnail, in the "Object Parameters" section and the subsection corresponding to the selected plot type.   

![Opacity Layer](../images/opacity_layer.png)

