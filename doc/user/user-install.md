# JSatOrb project: User installation procedure

## Pre-requisites

- JSatOrb is designed to be installed in a Linux Ubuntu 18.04 LTS 64 bit environment.
- Docker 19.03+ has to be available in the user environment.
- Docker compose 3.8+ has to be available in the user environment.
- The VTS/Prestoplot additional libraries have been installed by ISAE Administrators (see the "VTS additional libraries" paragraph below),
- The JSatorb installation archive has been provided to the user (__jsatorb-user-installation.tar.gz)__.


## Procedure

The procedure to install JSatOrb in the user environment is described below:

- uncompresses the JSatOrb user installation archive (tar.gz format) directly in __the user home directory__ (i.e. /home/[username]), with the following command:
```
>tar -zxvf jsatorb-user-installation.tar.gz
```
- go into the __JSatOrb__ folder of the uncompressed archive,
- launch the __jsatorb-load-docker-images.bash__ script in a terminal. It installs the JSatOrb Docker images in the local repository.


## IMPORTANT: Configuring Firefox to associate JSatOrb Agent with VZ files

---

**ATTENTION**

__Learn about the following information before launching JSatorb's GUI.__  

---

The first time the user will ask JSatOrb to produce a compressed file to be open in VTS, Firefox will not recognize the downloaded JSatOrb file format and will show the "Save or Open With..." popup dialog.

It is important to follow correctly the process described below on the first occurence of this case, as it may be more difficult to correct the Firefox configuratin afterward.

__This process has to be done only once and for all.__


When the Firefox "Save or Open With..." popup dialog opens the first time you want to displey JSatOrb data into the VTS software:
- select "Software" in the "Open with..." part of the dialog,
- __check the "always perform this action for this file type",
- confirm.

![Save or Open with Firefox dialog](../images/Firefox-save_or_openwith.png)  

Firefox will open the VZ file with the Software program in a separate window, which is not what we want, but now, we have to do the following steps:
- Close the "Software" window,
- Open the Firefox preferences (Main menu>Preferences),
- Go to the applications list (of the General tab),
- An entry should contain: 
    - content type: application/vnd+cssi.vtsproject+zip type
    - Action: Use Software
- Edit the asssociated action,
- Browse to your [home]/JSatOrb/JSatOrbAgent folder,
- Select the jsatorb-agent executable,
- Confirm.

![Firefox application preferences](../images/Firefox-preferences.png)  

__Now, VZ files produced by JSatOrb will be open by the JSatOrb Agent and forwarded to VTS.__


## Launching JSatOrb

Now, to use JSatOrb, the user only has to:
- launch the __jsatorb-start.bash__ script in a terminal,
- launch a Web browser and navigate to the URL: [http://localhost](http://localhost),
- The JSatOrb GUI should now displays in the navigator.

___Remark:___ __The mission data sets saved by the user thanks to the JSatOrb GUI are store in the ```/home/[username]/JSatOrb/mission-data/``` folder.__


## Troubleshooting

If nothing happens when trying to visualize JSatOrb data with the VTS software, here are some tips in order to overcome the issue.


### JSatOrb Agent's logs

The JSatOrb Agent logs can provide useful information when the VTS visualization doesn't work.
The logs are to be found in __`/home/[YOUR_USER_NAME]/JSatOrb/JSatOrbAgent/jsatorb-agent.log`__.

A possible issue is that the VTS additional libraries have not been installed in your environment, with a similar message as below:
```
error while loading shared libraries: libpng12.so.0: cannot open shared object file: No such file or directory
RETURN CODE 0
```

If it's the case, see the paragraph below to resolve this issue.


### VTS additional libraries

__The ISAE administrators are in charge of installing the VTS software and its additional libraries.__

However, if you note that nothing happens when trying to visualize JSatOrb data with the VTS software, the VTS additional libraries may be missing.

To check if those additional libraries/packages are available, run the following commands:
```
>apt-cache show libjpeg62
>apt-cache show libpng12-0
>apt-cache show gcc-multilib
>apt-cache show libx11-6:i386
```

__If one of those packages is missing, you have to:__

- Either call an ISAE Administrator (see the dev-detailed-install-procedure.md, or look into the shortened installation instructions below),
- Or if you are familiar with installing software through the apt tool, you can install the needed packages yourself:

    - Add an APT repository:
    ```
    >sudo add-apt-repository "deb http://mirrors.kernel.org/ubuntu/ xenial main"
    ```
    Wait a little bit, as it can take a few seconds to update.
    
    Then install the needed packages:
    ```
    >sudo apt update
    >sudo apt install libjpeg62
    >sudo apt install libpng12-0
    >sudo apt install gcc-multilib
    >sudo apt install libx11-6:i386
    ````