# JSatOrb Project: Mission Examples

This file describes two standard missions provided as examples. The examples are provided as .jso (JSatOrb mission files), and can be loaded from the mission manager tab.


## Sun-Synchronous Mission

The satellite describes a Sun-synchronous orbit around Earth, meaning that the precession rate of the orbit, mostly due to the Earth's J2, equals the mean motion of the Earth about the Sun. For a circular orbit, this links the inclination to the semi-major axis. For a 7000 km radius orbit, the inclination has to be equal to about 97.87°.

Since the J2 effect is currently not considered in the propagator, the orbit is not Sun-synchronous, but its parameters represent an orbit that would be Sun-synchronous if the precession rate was considered.

A ground station representing ISAE is also included in the mission.


## Mars Mission

The other example is a satellite orbiting Mars with a semi-major axis of about 6001 km (Mars radius is about 3 390 km). The orbit is almost circular, with a small eccentricity of 0.02.
