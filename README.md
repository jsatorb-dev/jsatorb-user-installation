# JSatorb project: user root documentation file

This README is the root of the whole JSatOrb documentation dedicated to users.
It gives directions about every subject useful to install and use JSatOrb.

# Installation

The user installation procedure is available [in this document](doc/user/user-install.md).


# VTS visualization tool

JSatOrb uses VTS (Visualization Toolkit for Space data) in order to visualize all the mission processings.
The VTS documentation can be found online on [the official website](https://logiciels.cnes.fr/fr/content/vts), but a specific JSatOrb complementary documentation is also [available here](doc/user/user-VTS.md).


# Mission examples

Two examples of mission data sets can be found in the **mission-examples** folder at the root of the uncompressed user installation archive: 

- Martian orbit mission data set example,
- Sun-synchronous orbit mission data set example.

These two mission examples are described in details in [this file](doc/user/user-missions-examples.md).

