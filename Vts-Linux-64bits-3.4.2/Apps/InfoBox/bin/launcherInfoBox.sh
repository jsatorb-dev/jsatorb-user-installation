#!/bin/sh
# ---------------------------------------------------------------------------- #
# launcherInfoBox.sh
#
# HISTORIQUE
# VERSION : 3.2 : FA : FA_437 : 07/12/2017 : InfoBox ne se lance pas si le port est different de 8888
# VERSION : 3.1 : DM : DM_385 : 06/02/2017 : Passage à Qt5
# VERSION : 2.7 : DM : DM_234 : 15/07/2015 : Création du plugin InfoBox
# FIN-HISTORIQUE
# Copyright © (2017) CNES All rights reserved
#
# VER : $Id: 
# ---------------------------------------------------------------------------- #

# Input parameter $1 is the path to the VTS project file
# Input parameter $2 is the client application ID
# Input parameter --serverport is optionnal (8888 by default)

# Default server port 
serverport=8888
vtsconfig="$1"
appid=$2
shift && shift

while [ "$1" != "" ]; do
	case "$1" in
		"--serverport")
			serverport=$2
			shift
			;;
	esac
	shift
done



echo $appid $vtsconfig $appid --serverport $serverport

# End of file
