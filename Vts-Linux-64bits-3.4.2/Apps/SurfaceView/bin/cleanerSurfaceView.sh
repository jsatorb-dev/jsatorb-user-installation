#!/bin/sh
# ---------------------------------------------------------------------------- #
# cleaner2dWin.sh
#
# HISTORIQUE
# VERSION : 3.1 : DM : DM_385 : 06/02/2017 : Passage à Qt5
# VERSION : 3.0 : DM : DM_300 : 05/07/2016 : Généralisation de la fenêtre 2D
# VERSION : 2.7 : DM : DM_225 : 15/07/2015 : 2dWin : chargement dynamique des tuiles
# FIN-HISTORIQUE
# Copyright © (2017) CNES All rights reserved
#
# VER : $Id: 
#
# Suppression des répertoires temporaires de VTS
# ---------------------------------------------------------------------------- #


# Chemin absolu vers le script
SCRIPT=$(readlink -f "$0")
# Chemin absolu du répertoire contenant le script
SCRIPTPATH=$(dirname "$SCRIPT")


OPTS_DONE=''
until [ "$OPTS_DONE" != "" ]
do
   case "$1" in
      # Ça ressemble à une option
      -*)
         case "$1" in
            "--clear")
               # Suppression des répertoires du cache WMS
               find "$SCRIPTPATH/.." -maxdepth 1 -type d -name "cache*" | while read i
               do
                  rm -fr "/$i"
               done

               # Code retour
               exit 0
               ;;

         esac
         ;;

      # Ça ne ressemble pas à une option : fin des options
      *)
         OPTS_DONE='true'
         ;;
   esac
done


# ---------------------------------------------------------------------------- #
